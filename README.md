Palex is a tool meant to help generate brighter (or darker) shades from a given color to help
fill out "gaps" in auto-generated color palettes (such as ones given by pywal). At the moment
it only supports grabbing colors from pywal, but this is due to change in the future.

##Installation

To install, simply clone the repository and run setup.py (requires python-setuptools):

```
$ git clone https://gitlab.com/ivakam/palette-extender.git
$ cd palette-extender
$ python setup.py install --optimize=1
```

Upcoming:
- Implement custom sourcing for color
- Rewrite argument parsing
