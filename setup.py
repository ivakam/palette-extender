"""palex - setup.py"""
import sys
import setuptools

try:
    import palex
except ImportError:
    print("error: palex requires Python 3.5 or greater.")
    sys.exit(1)

LONG_DESC = open('README.md').read()
VERSION = palex.__version__
DOWNLOAD = f"https://gitlab.com/ivakam/palette-extender/-/archive/{VERSION}/palette-extender-{VERSION}.tar.gz"

setuptools.setup(
    name="palex",
    version=VERSION,
    author="Ivar Kamsvåg",
    author_email="ivar.kamsvag@gmail.com",
    description="Extend existing color palettes",
    long_description_content_type="text/markdown",
    long_description=LONG_DESC,
    keywords="palex palette-generation wal colorscheme terminal-emulators changing-colorschemes",
    license="MIT",
    url="https://gitlab.com/ivakam/palette-extender",
    download_url=DOWNLOAD,
    classifiers=[
        "Environment :: X11 Applications",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    packages=["palex"],
    entry_points={"console_scripts": ["palex=palex.__main__:main"]},
    python_requires=">=3.5",
    include_package_data=True,
    zip_safe=False)
