import os, logging
from .color_utils import changeLuminance
from .settings import CACHE_DIR

def getBaseColor(alternative):
    """Grabs a base color from the users pywal output"""
    colorPath = os.path.join(CACHE_DIR, "colors")
    logging.info("Reading pywal colors from " + colorPath + "...")
    colorFile = open(colorPath)
    bg = colorFile.readlines()
    if alternative == -1:
        bg = bg[1]
    else:
        bg = bg[0]
    colorFile.close()
    logging.debug("Base color successfully acquired!")
    return bg


def getPalette(color, iterations, alternative, step):
    """Generates a palette from a single color"""
    logging.debug(f"Generating {iterations}-color palette using color {color} with step {step}...")
    palette = []
    for i in range(iterations):
        newColor = changeLuminance(color, alternative * step * (i + 1))
        if alternative == -1:
            palette.insert(0, newColor)
        else:
            palette.append(newColor)
    logging.debug("Palette successfully generated!")
    logging.debug(f"Palette: {palette}")
    return palette
