import os, sys, getopt, logging
from . import color, output

iterations = 3
step = 0.25
alternative = 1
HELP_STRING = f"""Palex - Usage:
    palex [options] [output formats]
    Example: palex -vi 3 --css

Options:
    --iterations [0..9] (-i): The amount of colors to output. Default is {iterations}

    --step [0..1]       (-s): The coefficient with which to increase the luminance by. Default is
    {step}
                          
    --alternative       (-a): Alternative mode - Takes a bright color and darkens it to generate a palette.
                        
    --verbose           (-v): Prints additional information.
                        
    --quiet             (-q): Prints less information.
"""


def getSysArgs(argv):
    """Fetch command line options"""
    try:
        opts, args = getopt.getopt(argv, "haqvi:s:", ["quiet", "verbose", "alternative", "help", "css", "xres", "iterations=", "step=",])
        return opts
    except getopt.GetoptError:
        logging.error("Bad usage. Exiting...")
        sys.exit(2)


def parseArgs(opts):
    """Parse command line options"""
    global step
    global iterations
    global alternative

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(HELP_STRING)
            sys.exit(0)
        if opt in ("-v", "--verbose"):
            logging.getLogger().setLevel(logging.DEBUG)
        if opt in ("-q", "--quiet"):
            logging.getLogger().disabled = True
            sys.stdout = sys.stderr = open(os.devnull, "w")
        if opt in ("-a", "--alternative"):
            alternative = -1
        if opt in ("-s", "--step"):
            try:
                step = float(arg)
            except e:
                logging.error("Bad argument for step (number between 0-1)")
                sys.exit(2)
            if not 0 < step <= 1:
                logging.error("Bad argument for step (number between 0-1)")
                sys.exit(2)
        if opt in ("-i", "--iterations"):
            try:
                iterations = int(arg)
            except e:
                logging.error("Bad argument for iterations (number between 0-9)")
                sys.exit(2)
            if not 0 < iterations < 10:
                logging.error("Bad argument for iterations (number between 0-9)")
                sys.exit(2)


def main():
    logging.basicConfig(format=("%(message)s"),
                        level=logging.INFO,
                        stream=sys.stdout)

    argv = sys.argv[1:]
    opts = getSysArgs(argv)
    parseArgs(opts)
    base = color.getBaseColor(alternative)
    palette = color.getPalette(base, iterations, alternative, step)
    output.output(opts, palette)
    logging.info("All done! Exiting...")


if __name__ == "__main__":
    main()
