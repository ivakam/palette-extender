import re

def validateHex(hexStr):
    """Check if a hex string is valid"""
    validatedHex = re.sub(r'[^0-9a-f]', '', hexStr)
    if len(hexStr) < 6:
        validatedHex = re.sub(r'(.)', '$1$1', validatedHex)
    return validatedHex


def changeLuminance(hexStr, lum):
    """Brighten a color by 1 * lum units"""
    validatedHex = validateHex(hexStr)
    if validatedHex == '000000':
          #If the color is completely black, we can not generate a brighter color,
          #so we change it to be a very dark gray.
          validatedHex = '0D0D0D'
    
    lum = lum or 0
    rgb = "#"
    c = 0
    
    for i in range(3):
        c = int(validatedHex[(i * 2):(i * 2) + 2], 16)
        c = round(min(max(0, c * (1 + lum)), 255))
        c = hex(c)[2:]
        rgb += c.zfill(2)
    return rgb
