import shutil, subprocess
from .settings import CACHE_DIR

def xrdb(file):
    """Merge the colors into the X db so new terminals use them."""
    file = file + ".Xresources"
    if shutil.which("xrdb"):
        subprocess.run(["xrdb", "-merge", "-quiet", file], check=False)
